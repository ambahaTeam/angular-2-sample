import 'angular2-universal-polyfills';
import 'angular2-universal-patch';
import 'zone.js';
import { createServerRenderer } from 'aspnet-prerendering';
import { enableProdMode } from '@angular/core';
import { platformNodeDynamic } from 'angular2-universal';
import { AppModule } from './app/app.module';
enableProdMode();
const platform = platformNodeDynamic();
export default createServerRenderer(params => {
    return new Promise((resolve, reject) => {
        const requestZone = Zone.current.fork({
            name: 'angular-universal request',
            properties: {
                baseUrl: '/',
                requestUrl: params.url,
                originUrl: params.origin,
                preboot: false,
                document: '<app></app>'
            },
            onHandleError: (parentZone, currentZone, targetZone, error) => {
                // If any error occurs while rendering the module, reject the whole operation
                reject(error);
                return true;
            }
        });
        return requestZone.run(() => platform.serializeModule(AppModule)).then(html => {
            resolve({ html: html });
        }, reject);
    });
});
//# sourceMappingURL=C:/Users/9-b-r/Documents/repositories/dotnet-angular-2-sample/Ambaha.Sample/src/Ambaha.Sample/ClientApp/boot-server.js.map