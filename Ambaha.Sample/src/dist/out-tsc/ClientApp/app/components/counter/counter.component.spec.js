import { CounterComponent } from './counter.component';
import { TestBed, async } from '@angular/core/testing';
let fixture;
describe('Counter component', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [CounterComponent] });
        fixture = TestBed.createComponent(CounterComponent);
        fixture.detectChanges();
    });
    it('should display a title', async(() => {
        const titleText = fixture.nativeElement.querySelector('h1').textContent;
        expect(titleText).toEqual('Counter');
    }));
    it('should start with count 0, then increments by 1 when clicked', async(() => {
        const countElement = fixture.nativeElement.querySelector('strong');
        expect(countElement.textContent).toEqual('0');
        const incrementButton = fixture.nativeElement.querySelector('button');
        incrementButton.click();
        fixture.detectChanges();
        expect(countElement.textContent).toEqual('1');
    }));
});
//# sourceMappingURL=C:/Users/9-b-r/Documents/repositories/dotnet-angular-2-sample/Ambaha.Sample/src/Ambaha.Sample/ClientApp/app/components/counter/counter.component.spec.js.map